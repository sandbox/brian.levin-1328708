/**
 * @author Brian Levin
 * @date November 1, 2011
 * @version 0.0.3
 * 
 */


$(document).ready(function() {
    
    $('<div id="dialogModal"></div>').insertAfter('#content'); //Create the div necessary for content insertion.
    
    $("#dialogModal").dialog({ //Set the variables necessary for creating the modal dialog.
        autoOpen: false,
        modal: true,
        height: 500,
        width: 900,
        resizable: false,
        draggable: false
    });
        
    $("a[class=t4]").click(function(){ //Capture the tag handler necessary to fire the code.
        var page = $(this).attr('href'); //Get the href from the clicked link.
        var title = $(this).attr('title'); //Get the title from the clicked link.
        
        $('body').css("overflow", "hidden"); //Hide any overflow that may result from the modular alert
        
        $("#dialogModal").html('<iframe id="modalIframeId" width="100%" height="100%" marginWidth="0" marginHeight="0" frameBorder="0" scrolling="auto" />').dialog("open"); //Set the iframe and open the dialog.
        $("#modalIframeId").attr("src",page); //Set the iframe src to the link href.
        $("#dialogModal").dialog("option", "title", title ); //Set the title of the dialog to the title of the link.
        
        $("#dialogModal").dialog( "option", "buttons", { //Add the popout button to the modular alert.
            "Pop Out": function() {
                $(this).dialog("close");
                window.open(page, title);
            }});
        
        $("#modalIframeId").load(function(){ //Check to make sure the iframe has finished loading.
            $("#modalIframeId").contents().find("#header").css("display","none"); //Remove the header from the iframe page. (Note: only works if the iframe src and the page have the same domain.)
            
        });
        
        return false;
    });

        
   return false;
});

